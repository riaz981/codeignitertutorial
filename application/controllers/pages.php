<?php
    class Pages extends CI_Controller{
        public function view($page = 'home')
        {

        	if ( !file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        	{
        		// Whoops, we don't have a page for that!
                echo "Hello!";
        		show_404();
        	}

        	$data['title'] = ucfirst($page); // Capitalize the first letter

        	$this->load->view('templates/header', $data);
        	$this->load->view('pages/'.$page, $data);
        	$this->load->view('templates/footer', $data);

            /*
            $this->load->library('twig');
            $data['title'] = "twig has now been loaded";
            $this->twig->display('view.html.twig',$data);
            */
        }
    }
?>
