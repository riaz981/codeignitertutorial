<?php

    class Property_model extends CI_Model{

            public function __construct(){
                $this->load->database();
            }

            public function insert_name(){
                $name['firstname'] = $this->input->get('firstname');
                $name['lastname'] = $this->input->get('lastname');

                $data['name'] = json_encode($name);

                return $this->db->insert('form',$data);
            }

    }

?>
